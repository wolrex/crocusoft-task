﻿namespace User.API.Models.Enums
{
    public enum Roles
    {
        SUPERADMIN = 1,
        ADMIN = 2,
        MODERATOR = 3,
        USER = 4,
    }
}
