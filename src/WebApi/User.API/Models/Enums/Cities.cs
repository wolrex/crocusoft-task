﻿namespace User.API.Models.Enums
{
    public enum Cities
    {
        BAKU = 1,
        SUMQAYIT = 2,
        MASALLI = 3,
        LƏNKƏRAN = 4
    }
}
