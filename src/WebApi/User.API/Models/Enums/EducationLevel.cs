﻿namespace User.API.Models.Enums
{
    public enum EducationLevel
    {
        ALİ = 1,
        NATAMAMALİ = 2,
        ORTA = 3,
        TAMORTA = 4,
        HEÇBİRİ = 5
    }
}
