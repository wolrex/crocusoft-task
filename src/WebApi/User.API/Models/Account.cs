﻿using System.ComponentModel.DataAnnotations;
using User.API.Models;
using User.API.Models.Enums;

namespace API.Models
{
    public class Account : BaseModel
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Username { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Name { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Surname { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Email { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Password { get; set; }
        public string RefreshToken { get; set; } = string.Empty;
        public DateTime TokenCreated { get; set; }
        public DateTime TokenExpires { get; set; }
        public Roles Role { get; set; }
    }
}