﻿using System.ComponentModel.DataAnnotations;

namespace User.API.Models
{
    public class Category : BaseModel
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Title { get; set; }
    }
}
