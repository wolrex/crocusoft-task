﻿using System.ComponentModel.DataAnnotations;

namespace User.API.Models
{
    public class Position : BaseModel
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Title { get; set; }
    }
}
