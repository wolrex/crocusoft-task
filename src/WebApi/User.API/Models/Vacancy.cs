﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using User.API.Models.Enums;

namespace User.API.Models
{
    public class Vacancy:BaseModel
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Title { get; set; }
        [Required]
        public int Salary { get; set; }
        [Required]
        public Cities City { get; set; }
        [Required]
        public EducationLevel EduLevel { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string WorkExperience { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string VacancyDateStart { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string VacancyDateEnd { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string RelevantPerson { get; set; }
        [Required]
        public string Information { get; set; }
        [Required]
        public string WorkHourStart { get; set; }
        [Required]
        public string WorkHourEnd { get; set; }
        [Required]
        public string Requirment { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public Guid CategoryId { get; set; }
        [Required]
        public Category Category { get; set; }
    }
}
