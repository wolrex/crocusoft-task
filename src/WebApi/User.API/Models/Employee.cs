﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using User.API.Models.Enums;

namespace User.API.Models
{
    public class Employee : BaseModel
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Name { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Surname { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Email { get; set; }
        public string Image { get; set; }
        [Required]
        public Guid PositionId { get; set; }
        [Required]
        public Position Position { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }

    }
}
