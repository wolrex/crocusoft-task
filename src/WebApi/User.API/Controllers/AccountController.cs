﻿using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using User.API.Data;
using User.API.DataTransferObject;

namespace User.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        public AppDbContext _context { get; set; }
        public AccountController(AppDbContext context)
        {
            _context = context;
        }
        [HttpGet]
        public IActionResult Index()
        {
            var data = _context.Accounts.ToList();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            return Ok(_context.Accounts.Find(id));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAccount(Guid id, [FromForm] AccountDTO DTO)
        {
            Account? data = await _context.Accounts.FindAsync(id);

            if (ModelState.IsValid)
            {
                data!.Name = DTO.Name;
                data!.Surname = DTO.Surname;
                data!.Email = DTO.Email;
                data!.Password = DTO.Password;
                data!.Role = DTO.Role;
            }

            _context.Accounts.Update(data);
            await _context.SaveChangesAsync();
            return Ok(data);
        }

    }
}
