﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using User.API.Data;
using User.API.DataTransferObject;
using User.API.Models;

namespace User.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        public AppDbContext _context { get; set; }
        public CategoryController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _context.Categories.ToList();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            return Ok(_context.Categories.Find(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategory([FromForm] CategoryDTO DTO)
        {
            Category model = new()
            {
                Title = DTO.Title,
            };
            if (ModelState.IsValid)
            {
                _context.Categories.Add(model);
                await _context.SaveChangesAsync();
            }
            return Ok(model);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCategory(Guid id, [FromForm] CategoryDTO DTO)
        {
            Category? data = await _context.Categories.FindAsync(id);

            if (ModelState.IsValid)
            {
                data!.Title = DTO.Title;
            }

            _context.Categories.Update(data);
            await _context.SaveChangesAsync();
            return Ok(data);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCategory(Guid id)
        {
            Category? data = await _context.Categories.FindAsync(id);
            if (data.Equals(null))
            {
                return BadRequest("Data could not be found!");
            }
            _context.Categories.Remove(data);
            _context.SaveChanges();
            return Ok();
        }


    }
}
