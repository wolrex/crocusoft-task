﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using User.API.Data;
using User.API.DataTransferObject;
using User.API.Models;

namespace User.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : ControllerBase
    {
        public AppDbContext _context { get; set; }
        public PositionController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _context.Positions.ToList();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            return Ok(_context.Positions.Find(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreatePosition([FromForm] PositionDTO DTO)
        {
            Position model = new()
            {
                Title = DTO.Title,
            };
            if (ModelState.IsValid)
            {
                _context.Positions.Add(model);
                await _context.SaveChangesAsync();
            }
            return Ok(model);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePosition(Guid id, [FromForm] PositionDTO DTO)
        {
            Position? data = await _context.Positions.FindAsync(id);

            if (ModelState.IsValid)
            {
                data!.Title = DTO.Title;
            }

            _context.Positions.Update(data);
            await _context.SaveChangesAsync();
            return Ok(data);
        }

        [HttpDelete]
        public async Task<IActionResult> DeletePosition(Guid id)
        {
            Position? data = await _context.Positions.FindAsync(id);
            if (data.Equals(null))
            {
                return BadRequest("Data could not be found!");
            }
            _context.Positions.Remove(data);
            _context.SaveChanges();
            return Ok();
        }


    }
}
