﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using User.API.Constants;
using User.API.Data;
using User.API.DataTransferObject;
using User.API.Extensions;
using User.API.Models;
using User.API.Utils;

namespace User.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        public AppDbContext _context { get; set; }
        public EmployeeController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _context.Employees.Include(v => v.Position).ToList();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var data = _context.Employees.Where(v => v.Id == id).Include(v => v.Position).First();
            return Ok(data);
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmployee([FromForm] EmployeeDTO DTO)
        {
            if (!DTO.ImageFile.IsSupported("image"))
            {
                ModelState.AddModelError(nameof(DTO.ImageFile), "File type is unsupported, please select image");
                return Ok();
            }
            if (DTO.ImageFile.IsGreatergivenMb(1))
            {
                ModelState.AddModelError(nameof(DTO.ImageFile), "File size cannot be greater than 1 mb");
                return Ok();
            }

            Employee model = new()
            {
                Name = DTO.Name,
                Surname = DTO.Surname,
                Email = DTO.Email,
                PositionId = DTO.PositionId,
                Image = FileUtils.Create(FileConstants.EmployeeImagePath, DTO.ImageFile)
        };

            if (ModelState.IsValid)
            {
                _context.Employees.Add(model);
                await _context.SaveChangesAsync();
            }
            return Ok(model);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEmployee(Guid id, [FromForm] EmployeeDTO DTO)
        {
            Employee? data = await _context.Employees.FindAsync(id);

            if (ModelState.IsValid)
            {
                data!.Name = DTO.Name;
                data.Surname = DTO.Surname;
                data.Email = DTO.Email;
                data.ImageFile = DTO.ImageFile;
                data.PositionId = DTO.PositionId;
            }

            _context.Employees.Update(data);
            await _context.SaveChangesAsync();
            return Ok(data);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteEmployee(Guid id)
        {
            var data = await _context.Employees.FindAsync(id);
            if (data.Equals(null))
            {
                return BadRequest("Data could not be found!");
            }
            _context.Employees.Remove(data);
            _context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("/api/employee/byposition/{id}")]
        public async Task<IActionResult> GetEmployeeByPositionId(Guid id)
        {
            var data = await _context.Employees.Where(v => v.PositionId == id).ToListAsync();
            return Ok(data);
        }

    }
}
