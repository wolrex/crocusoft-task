﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using User.API.Data;
using User.API.DataTransferObject;
using User.API.Models;
using User.API.Models.Enums;

namespace User.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VacancyController : ControllerBase
    {
        public AppDbContext _context { get; set; }
        public VacancyController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _context.Vacancies.Include(v => v.Category).ToList();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            var data = _context.Vacancies.Where(v => v.Id == id).Include(v => v.Category).First();
            return Ok(data);
        }

        [HttpPost]
        public async Task<IActionResult> CreateVacancy([FromForm] VacancyDTO DTO)
        {
            Vacancy model = new()
            {
                Title = DTO.Title,
                Salary = DTO.Salary,
                City = DTO.City,
                EduLevel = DTO.EduLevel,
                Email = DTO.Email,
                Information = DTO.Information,
                Phone = DTO.Phone,
                RelevantPerson = DTO.RelevantPerson,
                Requirment = DTO.Requirment,
                VacancyDateEnd = DTO.VacancyDateEnd,
                VacancyDateStart = DTO.VacancyDateStart,
                WorkExperience = DTO.WorkExperience,
                WorkHourEnd = DTO.WorkHourEnd,
                WorkHourStart = DTO.WorkHourStart,
                CategoryId = DTO.CategoryId,
            };

            if (ModelState.IsValid)
            {
                _context.Vacancies.Add(model);
                await _context.SaveChangesAsync();
            }
            return Ok(model);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateVacancy(Guid id, [FromForm]VacancyDTO DTO)
        {
            Vacancy? data = await _context.Vacancies.FindAsync(id);

            if(ModelState .IsValid)
            {
                data!.Title = DTO.Title;
                data.Email = DTO.Email;
                data.Phone = DTO.Phone;
                data.WorkHourEnd = DTO.WorkHourEnd;
                data.WorkHourStart = DTO.WorkHourStart;
                data.EduLevel = DTO.EduLevel;
                data.Information = DTO.Information;
                data.WorkExperience = DTO.WorkExperience;
                data.City = DTO.City;
                data.Salary = DTO.Salary;
                data.RelevantPerson = DTO.RelevantPerson;
                data.Requirment = DTO.Requirment;
                data.VacancyDateStart = DTO.VacancyDateStart;
                data.VacancyDateEnd = DTO.VacancyDateEnd;
                data.CategoryId = DTO.CategoryId;
            }

            _context.Vacancies.Update(data);
            await _context.SaveChangesAsync();
            return Ok(data);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteVacancy(Guid id)
        {
            Vacancy? data = await _context.Vacancies.FindAsync(id);
            if(data.Equals(null))
            {
                return BadRequest("Data could not be found!");
            }
            _context.Vacancies.Remove(data);
            _context.SaveChanges();
            return Ok();
        }
        [HttpGet]
        [Route("/api/vacancy/bycategory/{id}")]
        public async Task<IActionResult> GetVacancyByCategoryId(Guid id)
        {
            var data = await _context.Vacancies.Where(v => v.CategoryId == id).ToListAsync();
            return Ok(data);
        }
    }
}
