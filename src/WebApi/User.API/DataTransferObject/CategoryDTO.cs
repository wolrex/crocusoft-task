﻿using System.ComponentModel.DataAnnotations;

namespace User.API.DataTransferObject
{
    public class CategoryDTO
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Title { get; set; }
    }
}
