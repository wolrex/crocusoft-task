﻿using System.ComponentModel.DataAnnotations;

namespace User.API.DataTransferObject
{
    public class EmployeeDTO
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Name { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Surname { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Email { get; set; }
        [Required]
        public Guid PositionId { get; set; }
        public IFormFile ImageFile { get; set; }
    }
}
