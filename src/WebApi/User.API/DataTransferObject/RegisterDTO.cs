﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace User.API.DataTransferObject
{
    public class RegisterDTO
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Username { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Name { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Surname { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        [PasswordPropertyText]
        public string Password { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        [PasswordPropertyText]
        public string PasswordConfirm { get; set; }
    }
}
