﻿using System.ComponentModel.DataAnnotations;

namespace User.API.DataTransferObject
{
    public class ResponseDTO
    {
        [Required]
        public string JWToken { get; set; }
        [Required]
        public string RefreshToken { get; set; }
    }
}
