﻿using System.ComponentModel.DataAnnotations;

namespace User.API.DataTransferObject
{
    public class LoginDTO
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Username { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Password { get; set; }
    }
}
