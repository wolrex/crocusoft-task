﻿using System.ComponentModel.DataAnnotations;
using User.API.Models.Enums;

namespace User.API.DataTransferObject
{
    public class AccountDTO
    {
        [Required]
        [StringLength(maximumLength: 500)]
        public string Name { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Surname { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Email { get; set; }
        [Required]
        [StringLength(maximumLength: 500)]
        public string Password { get; set; }
        public Roles Role { get; set; }
    }
}
